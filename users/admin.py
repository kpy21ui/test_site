from django.contrib import admin
from users.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['username', 'last_name', 'first_name', 'email', 'thumbnail']
    fieldsets = (
        ('Main fields', {
           'fields': ('username', 'email', 'password', ('first_name', 'last_name'))
        }),
        ('Additional fields', {
            'fields': ('avatar', 'birthday', 'mobile_phone')
        }),
        ('Служебные поля', {
            'classes': ('collapse',),
            'fields': ('user_permissions', 'groups', 'is_active', 'is_superuser')
        }),

    )
    list_editable = ['email']
    search_fields = ['username']
    list_filter = ['is_superuser']
    actions = ['make_inactive_user']

    def make_inactive_user(self, request, queryset):
        rows_updated = queryset.update(is_active=False)
        if rows_updated == 1:
            message_bit = "1 user was"
        else:
            message_bit = "%s users were" % rows_updated
        self.message_user(request, "%s successfully marked as inactive." % message_bit)
    make_inactive_user.short_description = "Mark selected users as inactive"

admin.site.register(UserProfile, UserProfileAdmin)