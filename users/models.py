from django.contrib.auth.models import AbstractUser, User
from django.db import models
from datetime import datetime


class UserProfile(AbstractUser):
    avatar = models.ImageField(upload_to='images/users', blank=True, null=True, verbose_name="Аватар")
    birthday = models.DateField(blank=True, null=True, verbose_name="Дата рождения")
    mobile_phone = models.IntegerField(blank=True, null=True, verbose_name="Мобильный телефон")

    def thumbnail(self):
        return """<a href="/media/%s"><img border="0" alt="" src="/media/%s" height="40" /></a>""" \
               % (self.avatar.name, self.avatar.name)
    thumbnail.allow_tags = True

    def __str__(self):
        if self.email:
            return self.email
        else:
            return self.username

    class Meta():
        db_table = 'user_profile'
        verbose_name_plural = "Пользователи"
        verbose_name = "Пользователь"
